/** @type {import('next').NextConfig} */
module.exports = {
  reactStrictMode: true,
  async rewrites() {
    return [
      {
        source: '/api/:path*',
        destination: 'http://localhost:3000/:path*',
      },
    ]
  },
  images: {
    domains: ['localhost'],
  },
  env: {
    API_URL: process.env.API_URL,
    ETHERSCAN_URL: process.env.ETHERSCAN_URL,
  }
}