import {NextPageWithLayout} from "../src/types";
import AuthLayout from "../layouts/auth.layout";
import AuthView from "../components/Auth/AuthView";
import Link from "next/link";
import LoginForm from "../components/Auth/LoginForm";
import { authPageRequireCheck } from "../middleware/authCheck";

const Login: NextPageWithLayout = ({data}:any) => {
    return (
        <>
            <AuthView>
                <div className="form-top text-center">
                    <h2 className="register-title">Hello Again!</h2>
                    <h4 className="register-sub-title">Welcome Back</h4>
                </div>
                <LoginForm redirectUrl = {data.redirect}/>
                <div className="form-bottom text-center">
                    <Link href="/forget-password">
                        <a className="forget-password">Forgot
                            Password?</a>
                    </Link>

                    <p className="form-bottom-text">Already have account? <Link
                        href={`/signup${data.redirect ? '?redirect='+data.redirect : '' }`}>Sign-up</Link></p>
                </div>
            </AuthView>
        </>
    )
}

export const getServerSideProps = async (context : any) => {
    const { redirect } = context.query;
    const redirectUrl = redirect ? redirect : '/';
    const checkRedirect = await authPageRequireCheck(context.req,context.res);
    if(checkRedirect){
        return {
            redirect: {
                permanent: true,
                destination: redirectUrl,
            }
        }
    }
    const data = {
        redirect: redirectUrl
    }
    return { props: {data}};
}

Login.getLayout = AuthLayout

export default Login
