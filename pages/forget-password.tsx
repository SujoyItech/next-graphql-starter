import {NextPageWithLayout} from "../src/types";
import {useAuth} from "../src/contexts/AuthProvider";
import AuthLayout from "../layouts/auth.layout";
import AuthView from "../components/Auth/AuthView";
import Link from "next/link";
import LoginForm from "../components/Auth/LoginForm";

import Router from "next/router";

import {useEffect} from "react";
import ForgetPasswordForm from "../components/Auth/ForgetPasswordForm";

const ForgetPassword: NextPageWithLayout = () => {
    const {isAuthenticated} = useAuth();
    useEffect(()=>{
        if (isAuthenticated()){
            Router.push('/');
        }
    },[])
    return (
        <>
            <AuthView>
                <div className="form-top text-center">
                    <h2 className="register-title">Forgot Password?</h2>
                    <h4 className="register-sub-title">Don’t worry, it happen to the best of us!</h4>
                </div>
                <ForgetPasswordForm/>
                <div className="form-bottom text-center">
                    <p className="form-bottom-text">Already have account? <Link href="/login">Sign-in</Link></p>
                </div>
            </AuthView>
        </>
    )
}

ForgetPassword.getLayout = AuthLayout

export default ForgetPassword