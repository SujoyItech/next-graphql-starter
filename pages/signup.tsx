import {NextPageWithLayout} from "../src/types";

import AuthLayout from "../layouts/auth.layout";
import AuthView from "../components/Auth/AuthView";
import Link from "next/link";
import SignupForm from "../components/Auth/SignupForm";
import {authPageRequireCheck} from "../middleware/authCheck";

const Signup: NextPageWithLayout = ({data}:any) => {
    return (
        <>
            <AuthView>
                <div className="form-top text-center">
                    <h2 className="register-title">Welcome</h2>
                    <h4 className="register-sub-title">Register for free</h4>
                </div>
                <SignupForm redirectUrl = {data.redirect}/>
                <div className="form-bottom text-center">
                    <p className="form-bottom-text">Already have account? <Link
                        href="/login">Sign-in</Link></p>
                </div>
            </AuthView>
        </>
    )
}

export const getServerSideProps = async (context : any) => {
    const { redirect } = context.query;
    const redirectUrl = redirect ? redirect : '/';
    const checkRedirect = await authPageRequireCheck(context.req,context.res);
    if(checkRedirect){
        return {
            redirect: {
                permanent: true,
                destination: redirectUrl,
            }
        }
    }
    const data = {
        redirect: redirectUrl
    }
    return { props: {data}};
}

Signup.getLayout = AuthLayout

export default Signup
