import {NextPageWithLayout} from "../../src/types";
import EditProfileSection from "../../sections/profile/EditProfileSection";
import {useMeQuery} from "../../src/graphql/generated";
import {useEffect, useState} from "react";
import BreadCrumbSection from "../../sections/BreadCrumbSection";
import BasicLayout from "../../layouts/basic.layout";
import {getCookie} from "cookies-next";
import {user} from "../../src/ssr/data";
import ProfileSection from "../../sections/profile/ProfileSection";

const Edit: NextPageWithLayout = ({data}: any) => {
    return (
        <>
            <BreadCrumbSection page_title="Profile" title="Edit Profile"/>
            {data.user && <EditProfileSection user = {data ? data.user.me : undefined}/>}
        </>
    )
}
export const getServerSideProps = async ({req,res} : any) => {
    const token = getCookie('token', { req, res });
    const getUser = await user(token);
    const data = {
        user: getUser
    }
    return { props: {data}};
}
Edit.getLayout = BasicLayout
export default Edit;

