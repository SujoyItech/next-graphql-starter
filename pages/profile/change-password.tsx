import {NextPageWithLayout} from "../../src/types";
import ChangePasswordSection from "../../sections/profile/ChangePasswordSection";
import BasicLayout from "../../layouts/basic.layout";
import BreadCrumbSection from "../../sections/BreadCrumbSection";

const ChangePassword: NextPageWithLayout = () => {
    return (
        <>
            <BreadCrumbSection page_title="Profile" title="Change Password"/>
            <ChangePasswordSection/>
        </>
    )
}
ChangePassword.getLayout = BasicLayout
export default ChangePassword;
