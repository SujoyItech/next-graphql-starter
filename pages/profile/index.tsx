import {NextPageWithLayout} from "../../src/types";
import ProfileSection from "../../sections/profile/ProfileSection";
import BasicLayout from "../../layouts/basic.layout";
import BreadCrumbSection from "../../sections/BreadCrumbSection";
import {user} from "../../src/ssr/data";
import {getCookie} from "cookies-next";
import {checkSSRTokenValidity} from "../../middleware/authCheck";

const Profile: NextPageWithLayout = ({data}:any) => {
    return (
        <>
            <BreadCrumbSection page_title="Profile" title="Profile"/>
            {data.user && <ProfileSection user = {data ? data.user.me : undefined}/>}
        </>
    )
}
export const getServerSideProps = async ({req,res} : any) => {
    const token = getCookie('token', {req,res});
    const checkSSRToken = await checkSSRTokenValidity(req,res);
    const getUser = checkSSRToken ? await user(token) : null;
    const data = {
        user: getUser
    }
    return { props: {data}};
}
Profile.getLayout = BasicLayout
export default Profile;
