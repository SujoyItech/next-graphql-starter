import '../styles/css/plugins.css';
import '../styles/css/bootstrap.min.css';
import '../styles/css/style.css';
import '../styles/css/responsive.css';

import {AppPropsWithLayout} from "../src/types";
import {QueryClient, QueryClientProvider} from "react-query";
import { ReactQueryDevtools } from 'react-query/devtools'


const queryClient = new QueryClient();

import AuthProvider from "../src/contexts/AuthProvider";

import { ToastProvider} from 'react-toast-notifications';
import MediaProvider from "../src/contexts/MediaProvider";
import RouteGuard from "../src/Services/RouteGuard";

function NftApp({ Component, pageProps }: AppPropsWithLayout) {
  const getLayout = Component.getLayout ?? ((page) => page)

  return (
      <QueryClientProvider client={queryClient}>
          <ToastProvider autoDismiss={true}>
              <MediaProvider>
                  <AuthProvider>
                      <RouteGuard>
                          <>
                              {getLayout(<Component {...pageProps} />)}
                              <ReactQueryDevtools initialIsOpen={false} />
                          </>
                      </RouteGuard>
                  </AuthProvider>
              </MediaProvider>
          </ToastProvider>

      </QueryClientProvider>
  )
}

export default NftApp;