import {NextPageWithLayout} from "../src/types";
import {useAuth} from "../src/contexts/AuthProvider";
import AuthLayout from "../layouts/auth.layout";
import AuthView from "../components/Auth/AuthView";
import Link from "next/link";

import Router from "next/router";

import {useEffect} from "react";
import ResetPasswordForm from "../components/Auth/ResetPasswordForm";

const ResetPassword: NextPageWithLayout = () => {
    const {isAuthenticated} = useAuth();
    useEffect(()=>{
        if (isAuthenticated()){
            Router.push('/');
        }
    },[])
    return (
        <>
            <AuthView>
                <div className="form-top text-center">
                    <h2 className="register-title">Reset Password</h2>
                    <h4 className="register-sub-title">Reset your password from here!</h4>
                </div>
                <ResetPasswordForm/>
                <div className="form-bottom text-center">
                    <p className="form-bottom-text">Forget your code? <Link href="/forget-password">Send Code Again </Link></p>
                </div>
            </AuthView>
        </>
    )
}

ResetPassword.getLayout = AuthLayout

export default ResetPassword