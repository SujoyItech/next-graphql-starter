import {NextPageWithLayout} from "../src/types";
import BasicLayout from "../layouts/basic.layout";
import HeroBannerSection from "../sections/HeroBannerSection";
import CategorySection from "../sections/CategorySection";

const Home: NextPageWithLayout = () => {

    return (
        <>
            <HeroBannerSection/>
            <CategorySection/>
        </>
    )
}

Home.getLayout = BasicLayout

export default Home
