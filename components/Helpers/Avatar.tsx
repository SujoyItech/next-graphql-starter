import Gravatar from "react-gravatar";

export const Avatar = ({ url, name, size, email, className}: any) => {
    if (url) return (<img src={url} alt={name}/>);
    return (<Gravatar email={email} size={size??100} className={className} />);
}