import {ReactNode} from "react";

export default interface Props{
    children : ReactNode,
    className : any,
    [key : string] : any
}