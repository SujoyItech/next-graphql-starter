import {useField} from "formik";

const MySelect = ({ label, ...props } : any) => {
    const [field, meta] = useField(props);
    return (
        <div className="form-group">
            { label && <label htmlFor={props.id || props.name}>{label}</label> }
            <select {...field} {...props} />
            {meta.touched && meta.error ? (
                <div className="error">{meta.error}</div>
            ) : null}
        </div>
    );
};
export default MySelect;