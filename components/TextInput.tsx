import {ReactNode} from "react";

type Props = {
    [key : string] : any
}
export default function TextInput({children,...rest} : Props){
    return (
        <div className="form-group">
            <input {...rest} />
            {children}
        </div>
    )
}