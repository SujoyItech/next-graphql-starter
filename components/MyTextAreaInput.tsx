import {useField} from "formik";

export default function MyTextAreaInput({label,...props } : any){
    const [field,meta] = useField(props);
    return (
        <>
            <div className="form-group">
                { label && <label htmlFor={props.id || props.name}>{label}</label> }
                <textarea {...field} {...props}></textarea>
                {meta.touched && meta.error ? (
                    <div className="text-danger p-1">{meta.error}</div>
                ) : null}
            </div>
        </>
    )
}