import {Avatar} from "../Helpers/Avatar";
import {CopyAbleAddressComponent} from "../CopyAbleAddressComponent";

export default function ProfileCard({user}:any){
    return (
        <>
            <div className="authors-profile-area">
                <div className="author-image-area text-center">
                    <Avatar className="author-image" url={user.avatar} email={user.email} name={user.name} size={200}/>
                </div>
                <div className="author-info text-center">
                    <h3 className="author-name">{user.name}</h3>
                    <h4 className="author-id">{user.username}</h4>
                    <h4 className="has-code">
                        <CopyAbleAddressComponent address={user.walletAddress}/>
                    </h4>
                    <p className="author-content">{user.author_content}</p>
                </div>
                {/*<div className="authors-follow">*/}
                {/*    <div className="authors-follow-left">*/}
                {/*        <h4 className="flow-title">Followers</h4>*/}
                {/*        <h3 className="flower-count">5498</h3>*/}
                {/*    </div>*/}
                {/*    <div className="authors-follow-right">*/}
                {/*        <a href="#" className="follow-btn">Follow</a>*/}
                {/*    </div>*/}
                {/*</div>*/}
            </div>
        </>
    )
}
