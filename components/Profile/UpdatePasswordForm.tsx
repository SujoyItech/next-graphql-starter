import {Form, Formik} from "formik";
import * as Yup from "yup";
import MyTextInput from "../MyTextInput";
import {Alert} from "react-bootstrap";
import React, {useState} from "react";
import {useUpdatePasswordMutation} from "../../src/graphql/generated";
import {useToasts} from "react-toast-notifications";
import Router from "next/router";

export default function UpdatePasswordForm(){
    const { addToast } = useToasts();
    const[showAlert,setShowAlert] = useState(false);
    const[error,setError] = useState("");
    const updatePasswordMutation = useUpdatePasswordMutation();

    return(
        <>
            <Formik
                initialValues={{
                    oldPassword: '',
                    password: '',
                    passwordConfirm: ''
                }}
                validationSchema={Yup.object({
                    oldPassword: Yup.string()
                        .max(15, 'Must be 15 characters or less'),
                    password: Yup.string().min(6).required('Password is required'),
                    passwordConfirm: Yup.string()
                        .oneOf([Yup.ref('password'), null], 'Passwords must match'),
                })}
                onSubmit= { async (values, { setSubmitting }) => {
                    try {
                        const response = await updatePasswordMutation.mutateAsync(values);
                        if (response.updatePassword){
                            if (response.updatePassword.success === true){
                                localStorage.setItem('response-type','success');
                                localStorage.setItem('response-message',response.updatePassword.message);
                                Router.push('/profile');
                            }else{
                                addToast(response.updatePassword.message, { appearance: "error" });
                            }
                        }else {
                            addToast('Something went wrong!', { appearance: "error" });
                        }
                    }catch (error){
                        addToast('Something went wrong!', { appearance: "error" });
                    }
                }}
            >
                <Form>
                    <MyTextInput label="Old Password" name="oldPassword" className="form-control"  id="oldPassword" type="password"
                                 placeholder="Old Password"/>
                    <MyTextInput label="Password" name="password" className="form-control"  id="password" type="password" placeholder="Password"/>
                    <MyTextInput label="Confirm Password" name="passwordConfirm" className="form-control" type="password" id="passwordConfirm"
                                 placeholder="Confirm Password"/>

                    <button type="submit" className="primary-btn">Change Password</button>

                    {error && showAlert && (
                        <Alert variant="danger" show={showAlert}>
                            <span className="text-center">{error}</span>
                        </Alert>
                    )}
                </Form>
            </Formik>
        </>
    )
}