import { Formik, Form, useField } from 'formik';
import * as Yup from 'yup';
import MyTextInput from "../MyTextInput";
import {useAuth} from "../../src/contexts/AuthProvider";
import Router from 'next/router'
import {useToasts} from "react-toast-notifications";

export default function SignupForm({redirectUrl} : any){
    const { addToast } = useToasts();
    const {signUp} = useAuth();

    return (
        <>
            <Formik
                initialValues={{
                    username:'',
                    name: '',
                    email: '',
                    password: '',
                    confirm_password: '',
                }}
                validationSchema={Yup.object({
                    username: Yup.string()
                        .max(15, 'Must be 15 characters or less')
                        .required('Required'),
                    name: Yup.string()
                        .max(50, 'Must be 15 characters or less')
                        .required('Required'),
                    email: Yup.string()
                        .email('Invalid email address')
                        .required('Required'),
                    password: Yup.string().min(6).required('Password is required'),
                    confirm_password: Yup.string()
                        .oneOf([Yup.ref('password'), null], 'Passwords must match'),
                })}
                onSubmit= { async (values, { setSubmitting }) => {
                    try {
                        const response = await signUp(values);
                        if (response.success === true){
                            setSubmitting(false);
                            localStorage.setItem('response-type','success');
                            localStorage.setItem('response-message',response.message);
                            Router.push(redirectUrl);
                        }else {
                            addToast(response.message, { appearance: "error" });
                        }
                    }catch (error:any){
                        console.log(error.errors[0]?.extension.response.message)
                        addToast(error.errors[0]?.extension.response.message, { appearance: "error" });
                    }
                }}
            >
                <Form>
                    <MyTextInput name="username" className="form-control"  id="username" type="text" placeholder="Enter Username"/>
                    <MyTextInput name="name" className="form-control"  id="name" type="text" placeholder="Enter Name"/>
                    <MyTextInput name="email" className="form-control" type="email" placeholder="Enter Email"/>
                    <MyTextInput name="password" className="form-control" type="password" placeholder="Enter Password"/>
                    <MyTextInput name="confirm_password" className="form-control" type="password" placeholder="Confirm Password"/>
                    <button type="submit" className="register-btn mb-5">Submit</button>
                </Form>

            </Formik>
        </>
    )
}
