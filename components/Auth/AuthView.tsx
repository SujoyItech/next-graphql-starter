import {ReactNode, useEffect} from "react";
type Props = {
    children: ReactNode;
};
import newsLetterImage from "../../public/assets/images/newsletter-popup-image.png";
import Image from "next/image";
import {ShowToastAlert} from "../../src/Services/ToastAlertService";
export default function AuthView( { children } : Props ){
    ShowToastAlert();
    return (
        <>
            <div className="register-area">
                <div className="container">
                    <div className="register-wrap">
                        <div className="row align-items-center">
                            <div className="col-md-6">
                                <div className="register-left">
                                    <div className="register-form">
                                        {children}
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-6 d-none d-md-block">
                                <div className="register-right text-center">
                                    <Image src={newsLetterImage} alt="newsletter-popup-image"/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )

}