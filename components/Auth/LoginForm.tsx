import {useAuth} from "../../src/contexts/AuthProvider";
import {Formik,Form} from "formik";
import * as Yup from "yup";
import Router from "next/router";
import MyTextInput from "../MyTextInput";
import {useToasts} from "react-toast-notifications";
import {AddToastAlert} from "../../src/Services/ToastAlertService";
export default function LoginForm({redirectUrl} : any){
    const { addToast } = useToasts();
    const {signIn} = useAuth();
    return (
        <>
            <Formik
                initialValues={{
                    username: '',
                    password: '',
                }}
                validationSchema={Yup.object({
                    username: Yup.string()
                        .required('Email or Username is required'),
                    password: Yup.string().min(6).required('Password is required'),
                })}
                onSubmit= { async (values, { setSubmitting }) => {
                    try {
                        const response = await signIn(values);
                        if (response.success === true){
                            setSubmitting(false);
                            AddToastAlert(response.message,"success");
                            Router.push(redirectUrl)
                        }else {
                            addToast(response.message, { appearance: "error" });
                        }
                    }catch (error){
                        addToast("Something went wrong", { appearance: "error" });
                    }

                }}
            >
                <Form>
                    <MyTextInput name="username" className="form-control" type="text"
                                 placeholder="Enter email or username" />
                    <MyTextInput name="password" className="form-control" type="password"
                                 placeholder="Enter Password" />
                    <button type="submit" className="register-btn mb-5">Submit</button>
                </Form>

            </Formik>

        </>
    )
}
