import MediaCard from "./MediaCard";
import React, {useState} from "react";
import {useListFileQuery} from "../../src/graphql/generated";
import {useMedia} from "../../src/contexts/MediaProvider";

export default function MediaList(){
    const listFileQuery = useListFileQuery();
    const {setMediaImage,setFileContent} = useMedia();
    const [selectedImage, setSelectedImage] = useState<string | null>(null);
    const onImageSelected = (media : any)=>{
        setMediaImage(media.url);
        setSelectedImage(null);
        setSelectedImage(media.url)
        setFileContent(media);
    }
    return(
        <>
            <div className="row">
                {
                    listFileQuery.data?.listFile?
                        listFileQuery.data.listFile.map((media)=>{
                            return (
                                <div className="col-lg-3 col-md-6" key={media.name}>
                                    <div className="single-live-action">
                                        <div className="live-action-thumbnail">
                                            <img src={media.url} alt="live-action-image"/>
                                            <a onClick={()=>onImageSelected(media)} className={`primary-btn ${selectedImage && selectedImage === media.url ? 'success-btn' : ''}`}>
                                                <i className={selectedImage && selectedImage === media.url ? 'fas fa-check' : ''}></i>
                                                {selectedImage && selectedImage === media.url ? ' Selected' : ' Select'}
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            )
                        }) : 'Loading...'
                }
            </div>
        </>
    )
}
