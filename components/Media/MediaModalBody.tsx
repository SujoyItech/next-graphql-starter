import MyModal from "../MyModal";
import MediaList from "./MediaList";
import FileUploader from "./FileUploader";
import React, {useState} from "react";
import {useUploadFileMutation} from "../../src/graphql/generated";
import {useToasts} from "react-toast-notifications";

export default function MediaModalBody(){
    const uploadFileMutation = useUploadFileMutation();
    const [imageUploadModal, setImageUploadModal] = useState(false);
    const {addToast} = useToasts();
    const ImageUploadToggle = () => setImageUploadModal(!imageUploadModal);
    const[loading, setLoading] = useState(true);
    return (
        <>
            <div className="explore-page section">
                <div className="container">
                    <div className="search-filter-area mb-40">
                        <div className="row align-items-center">
                            <div className="col-lg-8 col-md-5 order-md-first order-last">
                                <a onClick={() => ImageUploadToggle()} className="primary-btn"><i className="fa fa-plus"></i> Creat Media</a>
                            </div>
                            <div className="col-lg-4 col-md-7 order-md-last order-first">
                                <form action="#">
                                    <div className="search-field">
                                        <input type="search" className="form-control" id="search" placeholder="Search"/>
                                        <button className="search-btn"><i className="fas fa-search"></i></button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    {loading && <MediaList/>}
                </div>
            </div>
            <MyModal show={imageUploadModal} title="Image Upload Modal" close={ImageUploadToggle}>

                <FileUploader
                    onFileUpload={async (files) => {
                        setLoading(false);
                        try {
                            if (files[0]){
                                const response = await uploadFileMutation.mutateAsync({file: files[0]});
                                if (response.uploadFile){
                                    addToast("New media uploaded successfully!", { appearance: "success" });
                                }else {
                                    addToast("New media upload failed!", { appearance: "error" });
                                }
                            }
                        }catch (error){
                            addToast("Something went wrong. Try again!", { appearance: "error" });
                        }

                        setImageUploadModal(false);
                        setLoading(true);

                    }}
                />
            </MyModal>
        </>
    )
}