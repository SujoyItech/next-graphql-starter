import React, { useState } from 'react';
import Link from "next/link";
import {Row, Col, Card, Image} from 'react-bootstrap';
import Dropzone from 'react-dropzone';
import liveImage from "../../public/assets/images/live-action-image-1.jpg"

interface FileType extends File {
    preview?: string;
    formattedSize?: string;
}

interface FileUploaderProps {
    onFileUpload?: (files: FileType[]) => void;
    showPreview?: boolean;
}

const FileUploader = (props: FileUploaderProps) => {
    const [selectedFiles, setSelectedFiles] = useState<FileType[]>([]);

    /**
     * Handled the accepted files and shows the preview
     */
    const handleAcceptedFiles = (files: FileType[]) => {
        var allFiles = files;

        if (props.showPreview) {
            (files || []).map((file) =>
                Object.assign(file, {
                    preview: file['type'].split('/')[0] === 'image' ? URL.createObjectURL(file) : null,
                    formattedSize: formatBytes(file.size),
                })
            );
            allFiles = [...selectedFiles];
            allFiles.push(...files);
            setSelectedFiles(allFiles);
        }

        if (props.onFileUpload) props.onFileUpload(allFiles);
    };

    /**
     * Formats the size
     */
    const formatBytes = (bytes: number, decimals: number = 2) => {
        if (bytes === 0) return '0 Bytes';
        const k = 1024;
        const dm = decimals < 0 ? 0 : decimals;
        const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];

        const i = Math.floor(Math.log(bytes) / Math.log(k));
        return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
    };

    /*
     * Removes the selected file
     */
    const removeFile = (fileIndex: number) => {
        const newFiles = [...selectedFiles];
        newFiles.splice(fileIndex, 1);
        setSelectedFiles(newFiles);
        if (props.onFileUpload) props.onFileUpload(newFiles);
    };


    return (
        <>
            <Dropzone {...props} onDrop={(acceptedFiles) => handleAcceptedFiles(acceptedFiles)}>
                {({ getRootProps, getInputProps }) => (
                    <div className="dropzone">
                        <div className="dz-message needsclick text-center" {...getRootProps()}>
                            <input {...getInputProps()} />
                            <button className="btn btn-success btn-lg text-center">Click here to upload.</button><br/>
                            <i className="fa 2x fa-cloud"></i><br/>
                            <span className="text-muted font-13">
                                (Drag and drop here to upload image)
                            </span>
                        </div>
                    </div>
                )}
            </Dropzone>

            {props.showPreview && (
                <div className="dropzone-previews mt-3" id="uploadPreviewTemplate">
                    {(selectedFiles || []).map((f, i) => {
                        return (
                            <Card className="" key={i + '-file'}>
                                <div className="p-2">
                                    <Row className="align-items-center p-2">
                                        {f.preview && (
                                            <Col className="col-md-12">
                                                <img
                                                    data-dz-thumbnail=""
                                                    className="dropzone"
                                                    alt={f.name}
                                                    src={f.preview}
                                                />
                                            </Col>
                                        )}
                                        {!f.preview && (
                                            <Col className="col-md-12">
                                                <div className="avatar-sm">
                                                    <span className="avatar-title bg-primary rounded">
                                                        {f.type.split('/')[0]}
                                                    </span>
                                                </div>
                                            </Col>
                                        )}
                                        <Col className="col-md-10">
                                            <Link href="/">
                                                <a className="text-muted fw-bold">
                                                    {f.name}
                                                </a>
                                            </Link>
                                            <p className="mb-0">
                                                <strong>{f.formattedSize}</strong>
                                            </p>
                                        </Col>
                                    </Row>
                                </div>
                            </Card>
                        );
                    })}
                </div>
            )}
        </>
    );
};

FileUploader.defaultProps = {
    showPreview: true,
};

export default FileUploader;
