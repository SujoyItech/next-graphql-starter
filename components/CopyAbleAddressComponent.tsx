import {collapseAddress} from "../src/lib/functions";
import {CopyToClipboard} from 'react-copy-to-clipboard';
import {useState} from "react";


export function CopyAbleAddressComponent({ address }: any) {
    const [copied, setCopied] = useState(false);
    const listener = () => {
        setCopied(true)
        setTimeout(() => {
            setCopied(false)
        }, 5000);
    }
    return (
        <CopyToClipboard text={address} onCopy={listener}>
            <h4 className="secret-code text-success">
                {collapseAddress(address)}
                {copied?<span className="font-weight-bold text-warning">...Copied</span>:null}
            </h4>
        </CopyToClipboard>
    )
}