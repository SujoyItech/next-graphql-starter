import PropTypes, {any} from 'prop-types';
import {createContext, ReactNode, useContext, useEffect, useState} from 'react';
import {SignUpInput,SignInInput} from '../Interfaces/AuthInterface';
import {useLoginMutation, useSignupMutation} from "../graphql/generated";
import { setCookies, getCookie, removeCookies } from 'cookies-next';

interface responseInterface{
    success : boolean,
    message : string | null,
    error?: any,
    data?: any
}
type authContextType = {
    authState: boolean;
    isAuthenticated : () => any,
    signUp: (payload : SignUpInput) => any;
    signIn: (payload : SignInInput) => any;
    signOut: () => any;
};

const authContextDefaultValues: authContextType = {
    authState: false,
    isAuthenticated: () => {return false},
    signUp: () => {return null},
    signIn: () => {return null},
    signOut: () => {return null},
};

type Props = {
    children: ReactNode;
    data : any
};

const AuthContext = createContext<authContextType>(authContextDefaultValues);

const AuthProvider = ({ children, data } : Props) => {
    const [authState , setAuthState] = useState(false);
    const signUpMutation = useSignupMutation();
    const loginMutation = useLoginMutation();

    useEffect(() => {
        if (isAuthenticated()) {
            setAuthState(true);
        }

    }, []);

    const signUp = async (payload : any) => {
        const data = {
            name : payload.name,
            username : payload.username,
            email : payload.email,
            password: payload.password
        }
        try {
            const signupResponse = await signUpMutation.mutateAsync(data);
            const token = {
                accessToken: signupResponse.signup.accessToken,
                refreshToken: signupResponse.signup.refreshToken,
                expiredAt: signupResponse.signup.expireAt,
            };
            setCookies('token',JSON.stringify(token));
            setAuthState(true);
            return {
                success : true,
                message : 'Signup successful!'
            }
        }catch (error:any){
            return {
                success : false,
                message :  error.message
            }
        }
    };

    const signIn = async (payload:any) => {
        try {
            const response = await loginMutation.mutateAsync(payload);
            console.log(response);
            const accessToken = response.login.accessToken
            const refreshToken = response.login.refreshToken;
            const expiredAt = response.login.expireAt;

            const token = {
                accessToken : accessToken,
                refreshToken: refreshToken,
                expiredAt: expiredAt
            }
            setCookies('token',JSON.stringify(token));
            setAuthState(true);
            return {
                success : true,
                message : 'Login successful!'
            }
        }catch (error:any){
            return {
                success : false,
                message : error.message
            }
        }
    };

    const isAuthenticated = ()=>{
        const token = getCookie('token');
        let tokenObject = null;
        if (typeof token === "string") {
           tokenObject = token ? JSON.parse(token) : null;
        }
        if (tokenObject){
            // @ts-ignore
            if (new Date(tokenObject.expiredAt) >= new Date(Date.now())) {
                return true;
            }else {
                removeCookies('token');
            }
            removeCookies('token');
        }
        return false;
    }

    const signOut = async () => {
        setAuthState(false);
        removeCookies('token');
    };

    return (
        <AuthContext.Provider value={{ authState, isAuthenticated, signUp, signIn, signOut }}>
            {children}
        </AuthContext.Provider>
    );
};



AuthProvider.propTypes = {
    children: PropTypes.object,
};

const useAuth = () => useContext(AuthContext);

export { AuthProvider as default, useAuth };
