import { useMutation, UseMutationOptions, useQuery, UseQueryOptions, useInfiniteQuery, UseInfiniteQueryOptions, QueryFunctionContext } from 'react-query';
import { graphqlFetcher } from '../lib/fetcher';
export type Maybe<T> = T | null;
export type InputMaybe<T> = Maybe<T>;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
  /** Date custom scalar type */
  Date: any;
  /** The `Upload` scalar type represents a file upload. */
  Upload: any;
};

export type Bid = {
  __typename?: 'Bid';
  amount: Scalars['Float'];
  /** Identifies the date and time when the object was created. */
  createdAt: Scalars['Date'];
  id: Scalars['Int'];
  item?: Maybe<Item>;
  itemId: Scalars['Int'];
  sender?: Maybe<User>;
  senderId: Scalars['Int'];
  status: Scalars['String'];
  /** Identifies the date and time when the object was last updated. */
  updatedAt: Scalars['Date'];
};

export type Category = {
  __typename?: 'Category';
  /** Identifies the date and time when the object was created. */
  createdAt: Scalars['Date'];
  id: Scalars['Int'];
  image: Scalars['String'];
  items?: Maybe<Array<Item>>;
  title: Scalars['String'];
  /** Identifies the date and time when the object was last updated. */
  updatedAt: Scalars['Date'];
};

export type CategoryCreateInput = {
  image?: InputMaybe<Scalars['String']>;
  title: Scalars['String'];
};

export type Collection = {
  __typename?: 'Collection';
  author?: Maybe<User>;
  authorId: Scalars['Int'];
  contractAddress?: Maybe<Scalars['String']>;
  contractSymbol?: Maybe<Scalars['String']>;
  coverImage?: Maybe<Scalars['String']>;
  /** Identifies the date and time when the object was created. */
  createdAt: Scalars['Date'];
  description?: Maybe<Scalars['String']>;
  id: Scalars['Int'];
  image?: Maybe<Scalars['String']>;
  items?: Maybe<Array<Item>>;
  name: Scalars['String'];
  note?: Maybe<Scalars['String']>;
  royalties: Scalars['Float'];
  status: Scalars['String'];
  /** Identifies the date and time when the object was last updated. */
  updatedAt: Scalars['Date'];
  volumeTraded: Scalars['Int'];
};

export type CollectionConnection = {
  __typename?: 'CollectionConnection';
  edges?: Maybe<Array<CollectionEdge>>;
  pageInfo: PageInfo;
  totalCount: Scalars['Int'];
};

export type CollectionEdge = {
  __typename?: 'CollectionEdge';
  cursor: Scalars['String'];
  node: Collection;
};

export type CollectionOrder = {
  direction: OrderDirection;
  field: CollectionOrderField;
};

/** Properties by which collection connections can be ordered. */
export enum CollectionOrderField {
  CreatedAt = 'createdAt',
  Id = 'id',
  Loyalties = 'loyalties',
  Name = 'name'
}

export type CollectionWithMeta = {
  __typename?: 'CollectionWithMeta';
  collection: Collection;
  itemCount: Scalars['Int'];
};

export type CollectorWithItem = {
  __typename?: 'CollectorWithItem';
  collector: User;
  items: ItemConnection;
};

export type CreateDto = {
  auctionEndAt?: InputMaybe<Scalars['Date']>;
  categoryId: Scalars['Int'];
  collectionId: Scalars['Int'];
  description?: InputMaybe<Scalars['String']>;
  image: Scalars['String'];
  levels?: InputMaybe<Array<RangeDto>>;
  mediaPath: Scalars['String'];
  name: Scalars['String'];
  price: Scalars['Float'];
  properties?: InputMaybe<Array<PropertyDto>>;
  stats?: InputMaybe<Array<RangeDto>>;
};

export type FileObject = {
  __typename?: 'FileObject';
  name: Scalars['String'];
  type: Scalars['String'];
  url: Scalars['String'];
  variants?: Maybe<Array<FileVariant>>;
};

export type FileVariant = {
  __typename?: 'FileVariant';
  type: Scalars['String'];
  url: Scalars['String'];
};

export type History = {
  __typename?: 'History';
  amount: Scalars['String'];
  /** Identifies the date and time when the object was created. */
  createdAt: Scalars['Date'];
  from?: Maybe<User>;
  fromId: Scalars['Int'];
  hash?: Maybe<Scalars['String']>;
  id: Scalars['Int'];
  isValid: Scalars['Boolean'];
  item?: Maybe<Item>;
  itemId: Scalars['Int'];
  to?: Maybe<User>;
  toId?: Maybe<Scalars['Int']>;
  type: Scalars['String'];
  /** Identifies the date and time when the object was last updated. */
  updatedAt: Scalars['Date'];
};

export type Item = {
  __typename?: 'Item';
  auctionEndAt?: Maybe<Scalars['Date']>;
  bids?: Maybe<Array<Bid>>;
  category?: Maybe<Category>;
  categoryId: Scalars['Int'];
  collection?: Maybe<Collection>;
  collectionId: Scalars['Int'];
  createdAt: Scalars['Date'];
  creator?: Maybe<User>;
  creatorId: Scalars['Int'];
  currentOwner?: Maybe<User>;
  currentOwnerId: Scalars['Int'];
  description?: Maybe<Scalars['String']>;
  externalUrl?: Maybe<Scalars['String']>;
  histories?: Maybe<Array<History>>;
  id: Scalars['Int'];
  image?: Maybe<Scalars['String']>;
  ipfsHash?: Maybe<Scalars['String']>;
  levels?: Maybe<Array<ItemLevel>>;
  likeCount: Scalars['Int'];
  likes?: Maybe<Array<Like>>;
  mediaPath?: Maybe<Scalars['String']>;
  mintedAt?: Maybe<Scalars['Date']>;
  name: Scalars['String'];
  price: Scalars['Float'];
  prices?: Maybe<Array<Price>>;
  properties?: Maybe<Array<ItemProperty>>;
  stats?: Maybe<Array<ItemStat>>;
  status: Scalars['String'];
  tokenId?: Maybe<Scalars['Int']>;
  unlockContentUrl?: Maybe<Scalars['String']>;
  updatedAt?: Maybe<Scalars['Date']>;
  viewCount: Scalars['Int'];
};

export type ItemConnection = {
  __typename?: 'ItemConnection';
  edges?: Maybe<Array<ItemEdge>>;
  pageInfo: PageInfo;
  totalCount: Scalars['Int'];
};

export type ItemEdge = {
  __typename?: 'ItemEdge';
  cursor: Scalars['String'];
  node: Item;
};

export type ItemLevel = {
  __typename?: 'ItemLevel';
  id: Scalars['ID'];
  item?: Maybe<Item>;
  itemId: Scalars['Int'];
  name: Scalars['String'];
  value: Scalars['Float'];
  valueof: Scalars['Float'];
};

export type ItemOrder = {
  direction: OrderDirection;
  field: ItemOrderField;
};

/** Properties by which Item connections can be ordered. */
export enum ItemOrderField {
  Id = 'id',
  MintedAt = 'mintedAt',
  Name = 'name',
  Price = 'price',
  TokenId = 'tokenId',
  View = 'view'
}

export type ItemProperty = {
  __typename?: 'ItemProperty';
  id: Scalars['ID'];
  item?: Maybe<Item>;
  itemId: Scalars['Int'];
  name: Scalars['String'];
  type: Scalars['String'];
};

export type ItemStat = {
  __typename?: 'ItemStat';
  id: Scalars['ID'];
  item?: Maybe<Item>;
  itemId: Scalars['Int'];
  name: Scalars['String'];
  value: Scalars['Float'];
  valueof: Scalars['Float'];
};

export type Like = {
  __typename?: 'Like';
  id: Scalars['ID'];
  item?: Maybe<Item>;
  itemId: Scalars['Int'];
  user?: Maybe<User>;
  userId: Scalars['Int'];
};

export type LoginInput = {
  password: Scalars['String'];
  username: Scalars['String'];
};

export type Mutation = {
  __typename?: 'Mutation';
  StaffLogin: Token;
  changePassword: ResponseMessageWithStatusModel;
  createCategory: Category;
  createCollection: Collection;
  createRole: ResponseMessageWithStatusModel;
  createStaff: Staff;
  createWallet: Scalars['String'];
  deleteCategory: Category;
  deleteRole: ResponseMessageWithStatusModel;
  deleteStaff: ResponseMessageWithStatusModel;
  deployCollection: Scalars['String'];
  enableAuction: Scalars['Boolean'];
  login: Token;
  makeWin: Scalars['Boolean'];
  placeBid: Bid;
  publishItem: Scalars['String'];
  refreshToken: Token;
  removeMyBid: Scalars['Boolean'];
  sendForgetPasswordMail: ResponseMessageWithStatusModel;
  sendVerificationMail: ResponseMessageWithStatusModel;
  signup: Token;
  storeItem: Item;
  toggleLike: Scalars['Boolean'];
  updateCategory: Category;
  updatePassword: ResponseMessageWithStatusModel;
  updateProfile: ResponseMessageWithStatusModel;
  updateRole: ResponseMessageWithStatusModel;
  updateStaff: ResponseMessageWithStatusModel;
  updateStaffPassword: ResponseMessageWithStatusModel;
  updateStaffProfile: ResponseMessageWithStatusModel;
  uploadFile: FileObject;
  verifyUserEmail: ResponseMessageWithStatusModel;
};


export type MutationStaffLoginArgs = {
  data: StaffLoginInput;
};


export type MutationChangePasswordArgs = {
  data: ResetPasswordInput;
};


export type MutationCreateCategoryArgs = {
  data: CategoryCreateInput;
};


export type MutationCreateCollectionArgs = {
  data: CollectionInput;
};


export type MutationCreateRoleArgs = {
  data: RoleInput;
};


export type MutationCreateStaffArgs = {
  data: StaffCreateInput;
};


export type MutationDeleteCategoryArgs = {
  id: Scalars['Int'];
};


export type MutationDeleteRoleArgs = {
  id: Scalars['Int'];
};


export type MutationDeleteStaffArgs = {
  id: Scalars['Int'];
};


export type MutationDeployCollectionArgs = {
  collectionId: Scalars['Int'];
};


export type MutationEnableAuctionArgs = {
  date: Scalars['Date'];
  itemId: Scalars['Int'];
};


export type MutationLoginArgs = {
  data: LoginInput;
};


export type MutationPlaceBidArgs = {
  amount: Scalars['Float'];
  itemId: Scalars['Int'];
};


export type MutationPublishItemArgs = {
  itemId: Scalars['Int'];
};


export type MutationRefreshTokenArgs = {
  token: Scalars['String'];
};


export type MutationRemoveMyBidArgs = {
  itemId: Scalars['Int'];
};


export type MutationSendForgetPasswordMailArgs = {
  email: Scalars['String'];
};


export type MutationSignupArgs = {
  data: SignupInput;
};


export type MutationStoreItemArgs = {
  data: CreateDto;
};


export type MutationToggleLikeArgs = {
  itemId: Scalars['Int'];
  liked: Scalars['Boolean'];
};


export type MutationUpdateCategoryArgs = {
  data: CategoryCreateInput;
  id: Scalars['Int'];
};


export type MutationUpdatePasswordArgs = {
  data: UpdatePasswordInput;
};


export type MutationUpdateProfileArgs = {
  data: UpdateProfileInput;
};


export type MutationUpdateRoleArgs = {
  data: RoleInput;
  id: Scalars['Int'];
};


export type MutationUpdateStaffArgs = {
  data: StaffUpdateInput;
  id: Scalars['Int'];
};


export type MutationUpdateStaffPasswordArgs = {
  data: UpdatePasswordInput;
};


export type MutationUpdateStaffProfileArgs = {
  data: StaffUpdateInput;
};


export type MutationUploadFileArgs = {
  file: Scalars['Upload'];
};


export type MutationVerifyUserEmailArgs = {
  code: Scalars['Int'];
};

/** Possible directions in which to order a list of items when provided an `orderBy` argument. */
export enum OrderDirection {
  Asc = 'asc',
  Desc = 'desc'
}

export type PageInfo = {
  __typename?: 'PageInfo';
  endCursor?: Maybe<Scalars['String']>;
  hasNextPage: Scalars['Boolean'];
  hasPreviousPage: Scalars['Boolean'];
  startCursor?: Maybe<Scalars['String']>;
};

export type Price = {
  __typename?: 'Price';
  amount: Scalars['Float'];
  /** Identifies the date and time when the object was created. */
  createdAt: Scalars['Date'];
  id: Scalars['Int'];
  item?: Maybe<Scalars['Float']>;
  itemId: Scalars['Int'];
  /** Identifies the date and time when the object was last updated. */
  updatedAt: Scalars['Date'];
};

export type PropertyDto = {
  name: Scalars['String'];
  type: Scalars['String'];
};

export type Query = {
  __typename?: 'Query';
  getCategories: Array<Category>;
  getCollection: CollectionWithMeta;
  getCollectionLists: CollectionConnection;
  getCollectorList: UserConnection;
  getCollectorWithItem: CollectorWithItem;
  getRole: Role;
  getRoles: Array<Role>;
  getSettings: SettingsDto;
  getStaff: Staff;
  getStaffLists: Array<Staff>;
  listFile: Array<FileObject>;
  listItems: ItemConnection;
  me: User;
  myCollectionLists: Array<Collection>;
  myCollections: CollectionConnection;
  ownedItems: ItemConnection;
  singleItem: Item;
  staff: Staff;
  walletBalance: Scalars['Float'];
};


export type QueryGetCollectionArgs = {
  address: Scalars['String'];
};


export type QueryGetCollectionListsArgs = {
  after?: InputMaybe<Scalars['String']>;
  before?: InputMaybe<Scalars['String']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<CollectionOrder>;
  paginateNumber?: InputMaybe<Scalars['Int']>;
  query?: InputMaybe<Scalars['String']>;
  skip?: InputMaybe<Scalars['Int']>;
};


export type QueryGetCollectorListArgs = {
  after?: InputMaybe<Scalars['String']>;
  before?: InputMaybe<Scalars['String']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  paginateNumber?: InputMaybe<Scalars['Int']>;
  skip?: InputMaybe<Scalars['Int']>;
};


export type QueryGetCollectorWithItemArgs = {
  address: Scalars['String'];
  after?: InputMaybe<Scalars['String']>;
  before?: InputMaybe<Scalars['String']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<ItemOrder>;
  paginateNumber?: InputMaybe<Scalars['Int']>;
  skip?: InputMaybe<Scalars['Int']>;
};


export type QueryGetRoleArgs = {
  id: Scalars['Int'];
};


export type QueryGetRolesArgs = {
  orderBy?: InputMaybe<RoleOrder>;
  query?: InputMaybe<Scalars['String']>;
};


export type QueryGetStaffArgs = {
  id: Scalars['Int'];
};


export type QueryListItemsArgs = {
  after?: InputMaybe<Scalars['String']>;
  before?: InputMaybe<Scalars['String']>;
  categoryId?: InputMaybe<Scalars['Int']>;
  collectionId?: InputMaybe<Scalars['Int']>;
  first?: InputMaybe<Scalars['Int']>;
  isLiveAuction?: InputMaybe<Scalars['Boolean']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<ItemOrder>;
  query?: InputMaybe<Scalars['String']>;
  skip?: InputMaybe<Scalars['Int']>;
};


export type QueryMyCollectionListsArgs = {
  orderBy?: InputMaybe<CollectionOrder>;
};


export type QueryMyCollectionsArgs = {
  after?: InputMaybe<Scalars['String']>;
  before?: InputMaybe<Scalars['String']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<CollectionOrder>;
  query?: InputMaybe<Scalars['String']>;
  skip?: InputMaybe<Scalars['Int']>;
};


export type QueryOwnedItemsArgs = {
  after?: InputMaybe<Scalars['String']>;
  before?: InputMaybe<Scalars['String']>;
  categoryId?: InputMaybe<Scalars['Int']>;
  collectionId?: InputMaybe<Scalars['Int']>;
  first?: InputMaybe<Scalars['Int']>;
  isLiveAuction?: InputMaybe<Scalars['Boolean']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<ItemOrder>;
  query?: InputMaybe<Scalars['String']>;
  skip?: InputMaybe<Scalars['Int']>;
};


export type QuerySingleItemArgs = {
  contractAddress: Scalars['String'];
  token: Scalars['Int'];
};

export type RangeDto = {
  name: Scalars['String'];
  value: Scalars['Float'];
  valueof: Scalars['Float'];
};

export type ResetPasswordInput = {
  code: Scalars['String'];
  email: Scalars['String'];
  password: Scalars['String'];
  passwordConfirm: Scalars['String'];
};

export type ResponseMessageWithStatusModel = {
  __typename?: 'ResponseMessageWithStatusModel';
  /** message */
  message: Scalars['String'];
  /** success */
  success: Scalars['Boolean'];
};

export type Role = {
  __typename?: 'Role';
  /** Identifies the date and time when the object was created. */
  createdAt: Scalars['Date'];
  id: Scalars['Int'];
  name: Scalars['String'];
  permissions?: Maybe<Scalars['String']>;
  /** Identifies the date and time when the object was last updated. */
  updatedAt: Scalars['Date'];
};

export type RoleOrder = {
  direction: Scalars['String'];
  field: Scalars['String'];
};

export type Section = {
  __typename?: 'Section';
  description?: Maybe<Scalars['String']>;
  title: Scalars['String'];
};

export type Sections = {
  __typename?: 'Sections';
  category: Section;
};

export type SettingsDto = {
  __typename?: 'SettingsDto';
  cur: Scalars['String'];
  sections: Sections;
};

export type SignupInput = {
  email: Scalars['String'];
  name: Scalars['String'];
  password: Scalars['String'];
  phone?: InputMaybe<Scalars['String']>;
  username: Scalars['String'];
};

export type Staff = {
  __typename?: 'Staff';
  avatar?: Maybe<Scalars['String']>;
  /** Identifies the date and time when the object was created. */
  createdAt: Scalars['Date'];
  email: Scalars['String'];
  id: Scalars['Int'];
  name: Scalars['String'];
  phone?: Maybe<Scalars['String']>;
  role?: Maybe<Role>;
  roleId?: Maybe<Scalars['Int']>;
  /** Identifies the date and time when the object was last updated. */
  updatedAt: Scalars['Date'];
  username: Scalars['String'];
};

export type StaffCreateInput = {
  avatar?: InputMaybe<Scalars['String']>;
  email: Scalars['String'];
  name: Scalars['String'];
  password: Scalars['String'];
  phone?: InputMaybe<Scalars['String']>;
  roleId?: InputMaybe<Scalars['Int']>;
  username: Scalars['String'];
};

export type StaffLoginInput = {
  password: Scalars['String'];
  username: Scalars['String'];
};

export type StaffUpdateInput = {
  avatar?: InputMaybe<Scalars['String']>;
  email?: InputMaybe<Scalars['String']>;
  name?: InputMaybe<Scalars['String']>;
  password?: InputMaybe<Scalars['String']>;
  phone?: InputMaybe<Scalars['String']>;
  roleId?: InputMaybe<Scalars['Int']>;
  username?: InputMaybe<Scalars['String']>;
};

export type Token = {
  __typename?: 'Token';
  /** JWT access token */
  accessToken: Scalars['String'];
  /** JWT expiration time */
  expireAt: Scalars['Date'];
  /** JWT refresh token */
  refreshToken: Scalars['String'];
};

export type UpdatePasswordInput = {
  oldPassword: Scalars['String'];
  password: Scalars['String'];
  passwordConfirm: Scalars['String'];
};

export type UpdateProfileInput = {
  author_content?: InputMaybe<Scalars['String']>;
  avatar?: InputMaybe<Scalars['String']>;
  email: Scalars['String'];
  name?: InputMaybe<Scalars['String']>;
  phone?: InputMaybe<Scalars['String']>;
  resetCode?: InputMaybe<Scalars['String']>;
  username: Scalars['String'];
};

export type User = {
  __typename?: 'User';
  author_content?: Maybe<Scalars['String']>;
  avatar?: Maybe<Scalars['String']>;
  collections?: Maybe<Array<Collection>>;
  /** Identifies the date and time when the object was created. */
  createdAt: Scalars['Date'];
  createdItems?: Maybe<Array<Item>>;
  email: Scalars['String'];
  emailVerifiedAt?: Maybe<Scalars['Date']>;
  id: Scalars['Int'];
  isEmailVerified: Scalars['Boolean'];
  name: Scalars['String'];
  ownedItems?: Maybe<Array<Item>>;
  phone?: Maybe<Scalars['String']>;
  resetCode?: Maybe<Scalars['String']>;
  /** Identifies the date and time when the object was last updated. */
  updatedAt: Scalars['Date'];
  username: Scalars['String'];
  walletAddress?: Maybe<Scalars['String']>;
};

export type UserConnection = {
  __typename?: 'UserConnection';
  edges?: Maybe<Array<UserEdge>>;
  pageInfo: PageInfo;
  totalCount: Scalars['Int'];
};

export type UserEdge = {
  __typename?: 'UserEdge';
  cursor: Scalars['String'];
  node: User;
};

export type CollectionInput = {
  coverImage?: InputMaybe<Scalars['String']>;
  description?: InputMaybe<Scalars['String']>;
  image?: InputMaybe<Scalars['String']>;
  name: Scalars['String'];
  royalties: Scalars['Float'];
};

export type RoleInput = {
  name: Scalars['String'];
  permissions?: InputMaybe<Scalars['String']>;
};

export type ChangePasswordMutationVariables = Exact<{
  email: Scalars['String'];
  code: Scalars['String'];
  password: Scalars['String'];
  passwordConfirm: Scalars['String'];
}>;


export type ChangePasswordMutation = { __typename?: 'Mutation', changePassword: { __typename?: 'ResponseMessageWithStatusModel', success: boolean, message: string } };

export type LoginMutationVariables = Exact<{
  username: Scalars['String'];
  password: Scalars['String'];
}>;


export type LoginMutation = { __typename?: 'Mutation', login: { __typename?: 'Token', accessToken: string, refreshToken: string, expireAt: any } };

export type SendForgetPasswordMailMutationVariables = Exact<{
  email: Scalars['String'];
}>;


export type SendForgetPasswordMailMutation = { __typename?: 'Mutation', sendForgetPasswordMail: { __typename?: 'ResponseMessageWithStatusModel', success: boolean, message: string } };

export type SignupMutationVariables = Exact<{
  name: Scalars['String'];
  email: Scalars['String'];
  username: Scalars['String'];
  password: Scalars['String'];
}>;


export type SignupMutation = { __typename?: 'Mutation', signup: { __typename?: 'Token', accessToken: string, refreshToken: string, expireAt: any } };

export type UpdatePasswordMutationVariables = Exact<{
  oldPassword: Scalars['String'];
  password: Scalars['String'];
  passwordConfirm: Scalars['String'];
}>;


export type UpdatePasswordMutation = { __typename?: 'Mutation', updatePassword: { __typename?: 'ResponseMessageWithStatusModel', success: boolean, message: string } };

export type UpdateProfileMutationVariables = Exact<{
  name: Scalars['String'];
  email: Scalars['String'];
  username: Scalars['String'];
  avatar: Scalars['String'];
  phone: Scalars['String'];
}>;


export type UpdateProfileMutation = { __typename?: 'Mutation', updateProfile: { __typename?: 'ResponseMessageWithStatusModel', success: boolean, message: string } };

export type UploadFileMutationVariables = Exact<{
  file: Scalars['Upload'];
}>;


export type UploadFileMutation = { __typename?: 'Mutation', uploadFile: { __typename?: 'FileObject', name: string, type: string, url: string } };

export type AppQueryVariables = Exact<{ [key: string]: never; }>;


export type AppQuery = { __typename?: 'Query', getSettings: { __typename?: 'SettingsDto', cur: string, sections: { __typename?: 'Sections', category: { __typename?: 'Section', title: string, description?: string | null | undefined } } } };

export type BalanceQueryVariables = Exact<{ [key: string]: never; }>;


export type BalanceQuery = { __typename?: 'Query', walletBalance: number };

export type CategoriesQueryVariables = Exact<{ [key: string]: never; }>;


export type CategoriesQuery = { __typename?: 'Query', getCategories: Array<{ __typename?: 'Category', id: number, title: string, image: string }> };

export type ListFileQueryVariables = Exact<{ [key: string]: never; }>;


export type ListFileQuery = { __typename?: 'Query', listFile: Array<{ __typename?: 'FileObject', name: string, type: string, url: string }> };

export type MeQueryVariables = Exact<{ [key: string]: never; }>;


export type MeQuery = { __typename?: 'Query', me: { __typename?: 'User', id: number, email: string, name: string, phone?: string | null | undefined, avatar?: string | null | undefined, isEmailVerified: boolean, username: string, walletAddress?: string | null | undefined } };


export const ChangePasswordDocument = `
    mutation ChangePassword($email: String!, $code: String!, $password: String!, $passwordConfirm: String!) {
  changePassword(
    data: {code: $code, email: $email, password: $password, passwordConfirm: $passwordConfirm}
  ) {
    success
    message
  }
}
    `;
export const useChangePasswordMutation = <
      TError = unknown,
      TContext = unknown
    >(options?: UseMutationOptions<ChangePasswordMutation, TError, ChangePasswordMutationVariables, TContext>) =>
    useMutation<ChangePasswordMutation, TError, ChangePasswordMutationVariables, TContext>(
      'ChangePassword',
      (variables?: ChangePasswordMutationVariables) => graphqlFetcher<ChangePasswordMutation, ChangePasswordMutationVariables>(ChangePasswordDocument, variables)(),
      options
    );
export const LoginDocument = `
    mutation Login($username: String!, $password: String!) {
  login(data: {username: $username, password: $password}) {
    accessToken
    refreshToken
    expireAt
  }
}
    `;
export const useLoginMutation = <
      TError = unknown,
      TContext = unknown
    >(options?: UseMutationOptions<LoginMutation, TError, LoginMutationVariables, TContext>) =>
    useMutation<LoginMutation, TError, LoginMutationVariables, TContext>(
      'Login',
      (variables?: LoginMutationVariables) => graphqlFetcher<LoginMutation, LoginMutationVariables>(LoginDocument, variables)(),
      options
    );
export const SendForgetPasswordMailDocument = `
    mutation SendForgetPasswordMail($email: String!) {
  sendForgetPasswordMail(email: $email) {
    success
    message
  }
}
    `;
export const useSendForgetPasswordMailMutation = <
      TError = unknown,
      TContext = unknown
    >(options?: UseMutationOptions<SendForgetPasswordMailMutation, TError, SendForgetPasswordMailMutationVariables, TContext>) =>
    useMutation<SendForgetPasswordMailMutation, TError, SendForgetPasswordMailMutationVariables, TContext>(
      'SendForgetPasswordMail',
      (variables?: SendForgetPasswordMailMutationVariables) => graphqlFetcher<SendForgetPasswordMailMutation, SendForgetPasswordMailMutationVariables>(SendForgetPasswordMailDocument, variables)(),
      options
    );
export const SignupDocument = `
    mutation Signup($name: String!, $email: String!, $username: String!, $password: String!) {
  signup(
    data: {email: $email, name: $name, username: $username, password: $password}
  ) {
    accessToken
    refreshToken
    expireAt
  }
}
    `;
export const useSignupMutation = <
      TError = unknown,
      TContext = unknown
    >(options?: UseMutationOptions<SignupMutation, TError, SignupMutationVariables, TContext>) =>
    useMutation<SignupMutation, TError, SignupMutationVariables, TContext>(
      'Signup',
      (variables?: SignupMutationVariables) => graphqlFetcher<SignupMutation, SignupMutationVariables>(SignupDocument, variables)(),
      options
    );
export const UpdatePasswordDocument = `
    mutation UpdatePassword($oldPassword: String!, $password: String!, $passwordConfirm: String!) {
  updatePassword(
    data: {oldPassword: $oldPassword, password: $password, passwordConfirm: $passwordConfirm}
  ) {
    success
    message
  }
}
    `;
export const useUpdatePasswordMutation = <
      TError = unknown,
      TContext = unknown
    >(options?: UseMutationOptions<UpdatePasswordMutation, TError, UpdatePasswordMutationVariables, TContext>) =>
    useMutation<UpdatePasswordMutation, TError, UpdatePasswordMutationVariables, TContext>(
      'UpdatePassword',
      (variables?: UpdatePasswordMutationVariables) => graphqlFetcher<UpdatePasswordMutation, UpdatePasswordMutationVariables>(UpdatePasswordDocument, variables)(),
      options
    );
export const UpdateProfileDocument = `
    mutation UpdateProfile($name: String!, $email: String!, $username: String!, $avatar: String!, $phone: String!) {
  updateProfile(
    data: {email: $email, name: $name, username: $username, phone: $phone, avatar: $avatar}
  ) {
    success
    message
  }
}
    `;
export const useUpdateProfileMutation = <
      TError = unknown,
      TContext = unknown
    >(options?: UseMutationOptions<UpdateProfileMutation, TError, UpdateProfileMutationVariables, TContext>) =>
    useMutation<UpdateProfileMutation, TError, UpdateProfileMutationVariables, TContext>(
      'UpdateProfile',
      (variables?: UpdateProfileMutationVariables) => graphqlFetcher<UpdateProfileMutation, UpdateProfileMutationVariables>(UpdateProfileDocument, variables)(),
      options
    );
export const UploadFileDocument = `
    mutation UploadFile($file: Upload!) {
  uploadFile(file: $file) {
    name
    type
    url
  }
}
    `;
export const useUploadFileMutation = <
      TError = unknown,
      TContext = unknown
    >(options?: UseMutationOptions<UploadFileMutation, TError, UploadFileMutationVariables, TContext>) =>
    useMutation<UploadFileMutation, TError, UploadFileMutationVariables, TContext>(
      'UploadFile',
      (variables?: UploadFileMutationVariables) => graphqlFetcher<UploadFileMutation, UploadFileMutationVariables>(UploadFileDocument, variables)(),
      options
    );
export const AppDocument = `
    query app {
  getSettings {
    cur
    sections {
      category {
        title
        description
      }
    }
  }
}
    `;
export const useAppQuery = <
      TData = AppQuery,
      TError = unknown
    >(
      variables?: AppQueryVariables,
      options?: UseQueryOptions<AppQuery, TError, TData>
    ) =>
    useQuery<AppQuery, TError, TData>(
      variables === undefined ? ['app'] : ['app', variables],
      graphqlFetcher<AppQuery, AppQueryVariables>(AppDocument, variables),
      options
    );
export const useInfiniteAppQuery = <
      TData = AppQuery,
      TError = unknown
    >(
      variables?: AppQueryVariables,
      options?: UseInfiniteQueryOptions<AppQuery, TError, TData>
    ) =>
    useInfiniteQuery<AppQuery, TError, TData>(
      variables === undefined ? ['app.infinite'] : ['app.infinite', variables],
      (metaData) => graphqlFetcher<AppQuery, AppQueryVariables>(AppDocument, {...variables, ...(metaData.pageParam ?? {})})(),
      options
    );

export const BalanceDocument = `
    query balance {
  walletBalance
}
    `;
export const useBalanceQuery = <
      TData = BalanceQuery,
      TError = unknown
    >(
      variables?: BalanceQueryVariables,
      options?: UseQueryOptions<BalanceQuery, TError, TData>
    ) =>
    useQuery<BalanceQuery, TError, TData>(
      variables === undefined ? ['balance'] : ['balance', variables],
      graphqlFetcher<BalanceQuery, BalanceQueryVariables>(BalanceDocument, variables),
      options
    );
export const useInfiniteBalanceQuery = <
      TData = BalanceQuery,
      TError = unknown
    >(
      variables?: BalanceQueryVariables,
      options?: UseInfiniteQueryOptions<BalanceQuery, TError, TData>
    ) =>
    useInfiniteQuery<BalanceQuery, TError, TData>(
      variables === undefined ? ['balance.infinite'] : ['balance.infinite', variables],
      (metaData) => graphqlFetcher<BalanceQuery, BalanceQueryVariables>(BalanceDocument, {...variables, ...(metaData.pageParam ?? {})})(),
      options
    );

export const CategoriesDocument = `
    query categories {
  getCategories {
    id
    title
    image
  }
}
    `;
export const useCategoriesQuery = <
      TData = CategoriesQuery,
      TError = unknown
    >(
      variables?: CategoriesQueryVariables,
      options?: UseQueryOptions<CategoriesQuery, TError, TData>
    ) =>
    useQuery<CategoriesQuery, TError, TData>(
      variables === undefined ? ['categories'] : ['categories', variables],
      graphqlFetcher<CategoriesQuery, CategoriesQueryVariables>(CategoriesDocument, variables),
      options
    );
export const useInfiniteCategoriesQuery = <
      TData = CategoriesQuery,
      TError = unknown
    >(
      variables?: CategoriesQueryVariables,
      options?: UseInfiniteQueryOptions<CategoriesQuery, TError, TData>
    ) =>
    useInfiniteQuery<CategoriesQuery, TError, TData>(
      variables === undefined ? ['categories.infinite'] : ['categories.infinite', variables],
      (metaData) => graphqlFetcher<CategoriesQuery, CategoriesQueryVariables>(CategoriesDocument, {...variables, ...(metaData.pageParam ?? {})})(),
      options
    );

export const ListFileDocument = `
    query ListFile {
  listFile {
    name
    type
    url
  }
}
    `;
export const useListFileQuery = <
      TData = ListFileQuery,
      TError = unknown
    >(
      variables?: ListFileQueryVariables,
      options?: UseQueryOptions<ListFileQuery, TError, TData>
    ) =>
    useQuery<ListFileQuery, TError, TData>(
      variables === undefined ? ['ListFile'] : ['ListFile', variables],
      graphqlFetcher<ListFileQuery, ListFileQueryVariables>(ListFileDocument, variables),
      options
    );
export const useInfiniteListFileQuery = <
      TData = ListFileQuery,
      TError = unknown
    >(
      variables?: ListFileQueryVariables,
      options?: UseInfiniteQueryOptions<ListFileQuery, TError, TData>
    ) =>
    useInfiniteQuery<ListFileQuery, TError, TData>(
      variables === undefined ? ['ListFile.infinite'] : ['ListFile.infinite', variables],
      (metaData) => graphqlFetcher<ListFileQuery, ListFileQueryVariables>(ListFileDocument, {...variables, ...(metaData.pageParam ?? {})})(),
      options
    );

export const MeDocument = `
    query me {
  me {
    id
    email
    name
    phone
    avatar
    isEmailVerified
    username
    walletAddress
  }
}
    `;
export const useMeQuery = <
      TData = MeQuery,
      TError = unknown
    >(
      variables?: MeQueryVariables,
      options?: UseQueryOptions<MeQuery, TError, TData>
    ) =>
    useQuery<MeQuery, TError, TData>(
      variables === undefined ? ['me'] : ['me', variables],
      graphqlFetcher<MeQuery, MeQueryVariables>(MeDocument, variables),
      options
    );
export const useInfiniteMeQuery = <
      TData = MeQuery,
      TError = unknown
    >(
      variables?: MeQueryVariables,
      options?: UseInfiniteQueryOptions<MeQuery, TError, TData>
    ) =>
    useInfiniteQuery<MeQuery, TError, TData>(
      variables === undefined ? ['me.infinite'] : ['me.infinite', variables],
      (metaData) => graphqlFetcher<MeQuery, MeQueryVariables>(MeDocument, {...variables, ...(metaData.pageParam ?? {})})(),
      options
    );
