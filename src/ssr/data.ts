import {graphqlFetcher} from "../lib/fetcher";
import {MeDocument, MeQuery, MeQueryVariables} from "../graphql/generated";
export async function user(token: any){
    const user = graphqlFetcher<MeQuery, MeQueryVariables>(MeDocument);
    return await user(token);
}
