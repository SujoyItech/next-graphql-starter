import ServicesConfig from "../config/services";

export function collapseAddress(address: string) {
    return `${address.substring(0, 7)}...${address.substring(address.length-8, address.length-1)}`
}

export function formatCount(number: number) {
    return number;
}

export function formatPrice(amount: number) {
    return amount;
}

export function etherScanUrl(suffix: string) {
    return ServicesConfig.ether_scan_url + suffix;
}