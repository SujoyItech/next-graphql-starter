import {useAppQuery, useCategoriesQuery} from "../src/graphql/generated";
import Slider, {CustomArrowProps} from "react-slick";
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';

const SlickArrowLeft = ({ currentSlide, slideCount, ...props }: CustomArrowProps) => (
    <i {...props} className={
        "slick-prev slick-arrow fas fa-angle-left" +
        (currentSlide === 0 ? " slick-disabled" : "")
    }/>
);

const SlickArrowRight = ({ currentSlide, slideCount, ...props }: CustomArrowProps) => (
    <i {...props} className={
        "slick-next slick-arrow fas fa-angle-right" +
        (currentSlide === slideCount??1 - 1 ? " slick-disabled" : "")
    }/>
);

const CategorySection = () => {
    const categoriesQueryResult = useCategoriesQuery();
    const settingsQueryResult = useAppQuery();
    const sliderSettings = {
        infinite: true,
        autoplay: true,
        speed: 500,
        autoplaySpeed: 3000,
        slidesToShow: 3,
        slidesToScroll: 1,
        dots: true,
        arrows: true,
        prevArrow: <SlickArrowLeft/>,
        nextArrow: <SlickArrowRight/>,
        responsive: [
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 481,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    };

    return (
        <section className="catagory-area section-top">
            <div className="container">
                <div className="row">
                    <div className="col-lg-6 offset-lg-3">
                        <div className="section-title text-center mb-45">
                            <h2 className="title mb-15">{settingsQueryResult.data?.getSettings?.sections?.category.title}</h2>
                            <p className="sub-title mb-0">{settingsQueryResult.data?.getSettings?.sections?.category.description}</p>
                        </div>
                    </div>
                </div>
                {
                    categoriesQueryResult.data?.getCategories?.length ?
                    <Slider {...sliderSettings}>
                        {
                            categoriesQueryResult.data?.getCategories?.map(cat => {
                                return (
                                    <div className="single-catagory text-center" key={cat.id}>
                                        <div className="catagory-thumbnail">
                                            <a href="#"><img src={cat.image} alt={cat.title}/></a>
                                        </div>
                                        <div className="catagory-info">
                                            <h3 className="catagory-title"><a href="#">{cat.title}</a></h3>
                                        </div>
                                    </div>
                                )
                            })
                        }
                    </Slider>:
                        null
                }
            </div>
        </section>
    )
}

export default CategorySection
