import Image from "next/image";
import walletLogo from "../public/assets/images/wallet.svg";
import userLogo from "../public/assets/images/user.svg";
import searchLogo from "../public/assets/images/search.svg";
import loginLogo from "../public/login.svg";
import logoutLogo from "../public/logout.svg";
import {useAuth} from "../src/contexts/AuthProvider";
import Link from 'next/link';
import Router from "next/router";
import {useEffect, useState} from "react";


export default function Account(){
   const {signOut,isAuthenticated} = useAuth();
   const [isAuth, setAuth] = useState(false);
   useEffect(()=>{
       setAuth(isAuthenticated());
   },[])

    const logout = async ()=>{
        await signOut();
        Router.push('/login');
    }

    return (
        <div className="header-right">
            <ul className="header-right-item">
                {
                    isAuth && (
                        <>
                            <li>
                                <Link href="/">
                                    <a>
                                        <Image src= {walletLogo} alt="wallet"/>
                                    </a>
                                </Link>
                            </li>
                            <li className="user-area">
                                <Link href="/">
                                    <a><Image src={userLogo} alt="user"/></a>
                                </Link>
                                <ul className="sub-menu-item">
                                    <li><Link href="/profile"><a><i className="fas fa-user"></i> Profile</a></Link></li>
                                    <li><Link href="/profile/change-password"><a><i className="fas fa-key"></i> Change Password</a></Link></li>
                                </ul>
                            </li>
                            <li><a onClick={logout}><Image src= {logoutLogo} alt="Logout"/></a></li>
                        </>
                    )
                }
                {
                    !isAuth && (
                        <li><Link href="/login"><a><Image src={loginLogo} alt="Login"/></a></Link></li>
                    )
                }

                <li><a href="#"><Image src={searchLogo} alt="search"/></a></li>
            </ul>
            <button className="menu-toggle"><i className="fas fa-bars"></i></button>
        </div>
    )
}
