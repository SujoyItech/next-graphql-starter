import logo from '../public/assets/images/logo.png'
import Image from "next/image";
import Account from "./Account";
import Link from 'next/link';
import {ShowToastAlert} from "../src/Services/ToastAlertService";

const HeaderSection = () => {
    ShowToastAlert();
    return (
        <header className="header-area">
            <div className="container">
                <div className="header-wrap">
                    <div className="brand-area">
                        <Link href="/">
                            <a>
                                <Image src= {logo} alt="logo"/>
                            </a>
                        </Link>
                    </div>
                    <nav className="menu-area">
                        <ul className="main-menu">
                            <li className="active"><Link href="/">Home</Link></li>
                        </ul>
                    </nav>
                   <Account/>
                </div>
            </div>

        </header>
    )
}


export default HeaderSection;
