import Link from "next/link";

export default function BreadCrumbSection({page_title,title,parent} : any){
    return (
        <section className="breadcrumb-area">
            <div className="container">
                <div className="breadcrumb-wrap text-center">
                    <h2 className="page-title">{page_title}</h2>
                    <ul className="breadcrumb-page">
                        <li><Link href="/">Home</Link></li>
                        {parent && <li>{parent}</li>}
                        <li>{title}</li>
                    </ul>
                </div>
            </div>
        </section>
    )
}