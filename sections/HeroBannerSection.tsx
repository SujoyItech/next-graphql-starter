import {useAuth} from "../src/contexts/AuthProvider";
const HeroBannerSection = () => {
    const {authState} = useAuth();
    return (
        <div className="hero-banner">
            <div className="container">
                <div className="row">
                    <div className="col-lg-6 col-md-8">
                        <div className="hero-banner-info">
                            <h1 className="banner-title">The purpose of lorem ipsum is to create a text.</h1>
                            <p className="banner-content">Unit of data stored on a digital ledger, called a blockchain,
                                that certifies a digital asset to be unique and therefore not interchangeable</p>
                            <a href="#" className="banner-btn">Explore</a>
                            {authState && <a href="#" className="banner-btn">Create</a>}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default HeroBannerSection;