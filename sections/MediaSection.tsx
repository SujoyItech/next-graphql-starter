import React, {useEffect, useState} from 'react';
import MyModal from "../components/MyModal";
import MediaModalBody from "../components/Media/MediaModalBody";
const MediaSection = () => {
    const [modal, setModal] = useState(false);
    const Toggle = () => setModal(!modal);
    return(
        <>
            <section className="breadcrumb-area">
                <div className="container">
                    <div className="breadcrumb-wrap text-center">
                        <h2 className="page-title">Media</h2>
                        <ul className="breadcrumb-page">
                            <li><a href="index.html">Home</a></li>
                            <li>My Media</li>
                        </ul>
                    </div>
                </div>
            </section>
            <div className="explore-page section">
                <div className="container">
                    <div className="search-filter-area mb-40">
                        <div className="row align-items-center">
                            <div className="col-lg-8 col-md-5 order-md-first order-last">
                                <a onClick={() => Toggle()} className="primary-btn"><i className="fa fa-plus"></i> Creat Media</a>
                            </div>
                            <div className="col-lg-4 col-md-7 order-md-last order-first">
                                <form action="#">
                                    <div className="search-field">
                                        <input type="search" className="form-control" id="search" placeholder="Search"/>
                                        <button className="search-btn"><i className="fas fa-search"></i></button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <MyModal show={modal} size="xl" title="Media Modal" close={Toggle}>
                <MediaModalBody/>
            </MyModal>
        </>
    )
};

export default MediaSection;
