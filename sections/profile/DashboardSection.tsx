import {useUser} from "../../src/contexts/UserProvider";

export default function DashboardSection(){
    return(
        <>
            <main className="dashboard-main-content">
                <div className="container-fluid">
                    <div className="dashboard-wrap row">
                        <div className="dashboard-left">
                            <div className="marketplace-view mb-30">
                                <h3 className="section-inner-title">Marketplace view’</h3>
                                <div id="marketplace-chart-view"></div>
                            </div>
                            <div className="transaction-history">
                                <div className="secondary-table">
                                    <div className="table-top-bar">
                                        <div className="table-top-wrap">
                                            <div className="table-top-left">
                                                <h4 className="table-section-title">Transaction History</h4>
                                            </div>
                                            <div className="search-field">
                                                <input type="search" className="form-control" id="search"
                                                       placeholder="Search"/>
                                                <button className="search-btn"><i className="fas fa-search"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="table-responsive">
                                        <table className="table">
                                            <thead>
                                            <tr>
                                                <th scope="col">Image</th>
                                                <th scope="col">Address</th>
                                                <th scope="col">From</th>
                                                <th scope="col">To</th>
                                                <th scope="col">Value</th>
                                                <th scope="col">Fee</th>
                                                <th scope="col">Time</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td><img className="avater-image" src="assets/images/avater.png"
                                                         alt="avater"/></td>
                                                <td><span>0x15c2adbe81d26a07d4...</span></td>
                                                <td><span>@Felis congue</span></td>
                                                <td><span>@ayoub fennouni</span></td>
                                                <td><span>2.5 Ether</span></td>
                                                <td><span>0.00216855</span></td>
                                                <td><span>12:34:08 PM</span></td>
                                            </tr>
                                            <tr>
                                                <td><img className="avater-image" src="assets/images/avater.png"
                                                         alt="avater"/></td>
                                                <td><span>0x15c2adbe81d26a07d4...</span></td>
                                                <td><span>@Felis congue</span></td>
                                                <td><span>@ayoub fennouni</span></td>
                                                <td><span>2.5 Ether</span></td>
                                                <td><span>0.00216855</span></td>
                                                <td><span>12:34:08 PM</span></td>
                                            </tr>
                                            <tr>
                                                <td><img className="avater-image" src="assets/images/avater.png"
                                                         alt="avater"/></td>
                                                <td><span>0x15c2adbe81d26a07d4...</span></td>
                                                <td><span>@Felis congue</span></td>
                                                <td><span>@ayoub fennouni</span></td>
                                                <td><span>2.5 Ether</span></td>
                                                <td><span>0.00216855</span></td>
                                                <td><span>12:34:08 PM</span></td>
                                            </tr>
                                            <tr>
                                                <td><img className="avater-image" src="assets/images/avater.png"
                                                         alt="avater"/></td>
                                                <td><span>0x15c2adbe81d26a07d4...</span></td>
                                                <td><span>@Felis congue</span></td>
                                                <td><span>@ayoub fennouni</span></td>
                                                <td><span>2.5 Ether</span></td>
                                                <td><span>0.00216855</span></td>
                                                <td><span>12:34:08 PM</span></td>
                                            </tr>
                                            <tr>
                                                <td><img className="avater-image" src="assets/images/avater.png"
                                                         alt="avater"/></td>
                                                <td><span>0x15c2adbe81d26a07d4...</span></td>
                                                <td><span>@Felis congue</span></td>
                                                <td><span>@ayoub fennouni</span></td>
                                                <td><span>2.5 Ether</span></td>
                                                <td><span>0.00216855</span></td>
                                                <td><span>12:34:08 PM</span></td>
                                            </tr>
                                            <tr>
                                                <td><img className="avater-image" src="assets/images/avater.png"
                                                         alt="avater"/></td>
                                                <td><span>0x15c2adbe81d26a07d4...</span></td>
                                                <td><span>@Felis congue</span></td>
                                                <td><span>@ayoub fennouni</span></td>
                                                <td><span>2.5 Ether</span></td>
                                                <td><span>0.00216855</span></td>
                                                <td><span>12:34:08 PM</span></td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="dashboard-right">
                            <div className="row">
                                <div className="col-lg-12 col-sm-6 order-last order-lg-first">
                                    <div className="my-wallet-area mb-40">
                                        <h3 className="section-inner-title">My Wallet</h3>
                                        <div className="my-wallet-card">
                                            <div className="signle-card">
                                                <div className="balance-setion">
                                                    <h4>Balance</h4>
                                                    <h2>$521,652</h2>
                                                </div>
                                                <div className="monthly-Profit">
                                                    <h4>Monthly Profit</h4>
                                                    <h3>$14,225</h3>
                                                </div>
                                                <span className="percent">+10%</span>
                                            </div>
                                            <div className="signle-card">
                                                <div className="balance-setion">
                                                    <h4>Balance</h4>
                                                    <h2>$521,652</h2>
                                                </div>
                                                <div className="monthly-Profit">
                                                    <h4>Monthly Profit</h4>
                                                    <h3>$14,225</h3>
                                                </div>
                                                <span className="percent">+10%</span>
                                            </div>
                                            <div className="signle-card">
                                                <div className="balance-setion">
                                                    <h4>Balance</h4>
                                                    <h2>$521,652</h2>
                                                </div>
                                                <div className="monthly-Profit">
                                                    <h4>Monthly Profit</h4>
                                                    <h3>$14,225</h3>
                                                </div>
                                                <span className="percent">+10%</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-lg-12 col-sm-6 order-first order-lg-last">
                                    <div className="recent-activity">
                                        <h3 className="section-inner-title">Recent Activity</h3>
                                        <ul className="recent-activity-list">
                                            <li className="single-activity">
                                                <div className="activity-left">
                                                    <div className="user-area">
                                                        <img src="assets/images/author.png" alt="avater"
                                                             className="user-image"/>
                                                        <div className="user-info">
                                                            <h5 className="user-desc">Creative Art collection</h5>
                                                            <h4 className="user-name"><a href="#">Alexander Garg</a>
                                                            </h4>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="activity-right">
                                                    <span>2.81 ETH </span>
                                                    <span>2 days</span>
                                                </div>
                                            </li>
                                            <li className="single-activity">
                                                <div className="activity-left">
                                                    <div className="user-area">
                                                        <img src="assets/images/author.png" alt="avater"
                                                             className="user-image"/>
                                                        <div className="user-info">
                                                            <h5 className="user-desc">Creative Art collection</h5>
                                                            <h4 className="user-name"><a href="#">Alexander Garg</a>
                                                            </h4>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="activity-right">
                                                    <span>2.81 ETH </span>
                                                    <span>2 days</span>
                                                </div>
                                            </li>
                                            <li className="single-activity">
                                                <div className="activity-left">
                                                    <div className="user-area">
                                                        <img src="assets/images/author.png" alt="avater"
                                                             className="user-image"/>
                                                        <div className="user-info">
                                                            <h5 className="user-desc">Creative Art collection</h5>
                                                            <h4 className="user-name"><a href="#">Alexander Garg</a>
                                                            </h4>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="activity-right">
                                                    <span>2.81 ETH </span>
                                                    <span>2 days</span>
                                                </div>
                                            </li>
                                            <li className="single-activity">
                                                <div className="activity-left">
                                                    <div className="user-area">
                                                        <img src="assets/images/author.png" alt="avater"
                                                             className="user-image"/>
                                                        <div className="user-info">
                                                            <h5 className="user-desc">Creative Art collection</h5>
                                                            <h4 className="user-name"><a href="#">Alexander Garg</a>
                                                            </h4>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="activity-right">
                                                    <span>2.81 ETH </span>
                                                    <span>2 days</span>
                                                </div>
                                            </li>
                                            <li className="single-activity">
                                                <div className="activity-left">
                                                    <div className="user-area">
                                                        <img src="assets/images/author.png" alt="avater"
                                                             className="user-image"/>
                                                        <div className="user-info">
                                                            <h5 className="user-desc">Creative Art collection</h5>
                                                            <h4 className="user-name"><a href="#">Alexander Garg</a>
                                                            </h4>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="activity-right">
                                                    <span>2.81 ETH </span>
                                                    <span>2 days</span>
                                                </div>
                                            </li>
                                            <li className="single-activity">
                                                <div className="activity-left">
                                                    <div className="user-area">
                                                        <img src="assets/images/author.png" alt="avater"
                                                             className="user-image"/>
                                                        <div className="user-info">
                                                            <h5 className="user-desc">Creative Art collection</h5>
                                                            <h4 className="user-name"><a href="#">Alexander Garg</a>
                                                            </h4>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="activity-right">
                                                    <span>2.81 ETH </span>
                                                    <span>2 days</span>
                                                </div>
                                            </li>
                                            <li className="single-activity">
                                                <div className="activity-left">
                                                    <div className="user-area">
                                                        <img src="assets/images/author.png" alt="avater"
                                                             className="user-image"/>
                                                        <div className="user-info">
                                                            <h5 className="user-desc">Creative Art collection</h5>
                                                            <h4 className="user-name"><a href="#">Alexander Garg</a>
                                                            </h4>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="activity-right">
                                                    <span>2.81 ETH </span>
                                                    <span>2 days</span>
                                                </div>
                                            </li>
                                            <li className="single-activity">
                                                <div className="activity-left">
                                                    <div className="user-area">
                                                        <img src="assets/images/author.png" alt="avater"
                                                             className="user-image"/>
                                                        <div className="user-info">
                                                            <h5 className="user-desc">Creative Art collection</h5>
                                                            <h4 className="user-name"><a href="#">Alexander Garg</a>
                                                            </h4>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="activity-right">
                                                    <span>2.81 ETH </span>
                                                    <span>2 days</span>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </main>
        </>
    )
}