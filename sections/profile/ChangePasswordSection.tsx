import UpdatePasswordForm from "../../components/Profile/UpdatePasswordForm";

export default function ChangePasswordSection(){
    return (
        <>
            <div className="explore-page section">
                <div className="container">
                    <div className="dashbord-form">
                        <div className="row">
                            <div className="col-lg-8 offset-lg-2">
                                <h2 className="dashbord-section-title">Reset Password</h2>
                                <UpdatePasswordForm/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}
