import Link from "next/link";

import {Avatar} from "../../components/Helpers/Avatar";
import {CopyAbleAddressComponent} from "../../components/CopyAbleAddressComponent";

export default function ProfileSection({user} : any){

    return (
        <>
            <div className="explore-page section">
                <div className="container">
                    <div className="profile-page-area">
                        <div className="row">
                            <div className="col-md-4 col-lg-3">
                                <div className="profile-page-left">
                                    <h2 className="dashbord-section-title">My Profile</h2>
                                    <div className="profile-image">
                                        <Avatar url={user.avatar} email={user.email} name={user.name}/>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-8 col-lg-9">
                                <div className="profile-page-right">
                                    <h2 className="dashbord-section-title">General Information</h2>
                                    <div className="section-inner-wrap">
                                        <h3 className="section-inner-title">Profile Information</h3>
                                        <div className="profile-info">
                                            <div className="table-responsive">
                                                <table className="table">
                                                    <tbody>
                                                    <tr>
                                                        <td>Name</td>
                                                        <td>:</td>
                                                        <td><strong>{user.name}</strong></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Email</td>
                                                        <td>:</td>
                                                        <td>{user.email}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Phone</td>
                                                        <td>:</td>
                                                        <td>{user.phone}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Wallet Address</td>
                                                        <td>:</td>
                                                        <td><span><CopyAbleAddressComponent address={user.walletAddress}/></span></td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="sectoin-bottom text-center">
                                        <Link href="/profile/edit">
                                            <a className="primary-btn">Edit Profile</a>
                                        </Link>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}
