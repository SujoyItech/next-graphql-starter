import EditProfileForm from "../../components/Profile/EditProfileForm";

export default function EditProfileSection({user} : any){
    return (
        <>
            <div className="explore-page section">
                <div className="container">
                    <div className="profile-page-area">
                        <EditProfileForm user = {user}/>
                    </div>
                </div>
            </div>
        </>
    )
}
