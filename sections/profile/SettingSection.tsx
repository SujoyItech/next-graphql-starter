export default function SettingSection(){
    return (
        <>
            <main className="dashboard-main-content">
                <div className="container-fluid">
                    <div className="dashboard-sectoin-wrap">

                        <div className="dashbord-form">
                            <div className="row">
                                <div className="col-lg-8 offset-lg-2">
                                    <h2 className="dashbord-section-title">Reset Password</h2>
                                    <form action="#">
                                        <div className="form-group">
                                            <label className="label-text" htmlFor="o_password">Old Password</label>
                                            <input type="password" className="form-control" id="o_password"
                                                   autoComplete="off" placeholder="Enter Your Old Password . . ."/>
                                        </div>
                                        <div className="form-group">
                                            <label className="label-text" htmlFor="n_password">New Password</label>
                                            <input type="password" className="form-control" id="n_password"
                                                   autoComplete="off" placeholder="Enter New Password . . ."/>
                                        </div>
                                        <div className="form-group">
                                            <label className="label-text" htmlFor="c_password">Confirm Password</label>
                                            <input type="email" className="form-control" id="c_password"
                                                   autoComplete="off" placeholder="Re-Enter your New Password . . ."/>
                                        </div>
                                        <button type="submit" className="primary-btn">Change Password</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </main>
        </>
    )
}