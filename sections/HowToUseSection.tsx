const HowToUseSection = () => {
    return (
        <section className="how-to-use section section-bg">
            <div className="container">
                <div className="row">
                    <div className="col-lg-6 offset-lg-3">
                        <div className="section-title text-center mb-45">
                            <h2 className="title mb-15">Create & sell your NFT</h2>
                            <p className="sub-title mb-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed
                                quis accumsan nisi Ut ut felis congue nisl hendrerit commodo.</p>
                        </div>
                    </div>
                </div>
                <div className="how-to-use-left">
                    <div className="use-img-wrap">
                        <img className="thumbnail-img image-one" src="assets/images/use-img.png" alt="how to use"/>
                    </div>
                </div>
                <div className="row">
                    <div className="col-lg-8 offset-lg-4">
                        <div className="process-area">
                            <div className="row">
                                <div className="col-md-6 col-sm-6">
                                    <div className="single-process">
                                        <img className="process-icon" src="assets/images/process-wallet.svg"
                                             alt="wallet"/>
                                        <h3 className="process-title">Set up your wallet</h3>
                                        <p className="process-content">It is a long established fact reader will be
                                            distracted by the content ofpage when looking at its layout.</p>
                                    </div>
                                </div>
                                <div className="col-md-6 col-sm-6">
                                    <div className="single-process">
                                        <img className="process-icon" src="assets/images/creative-process.svg"
                                             alt="creative-process"/>
                                        <h3 className="process-title">Create your collection</h3>
                                        <p className="process-content">It is a long established fact reader will be
                                            distracted by the content ofpage when looking at its layout.</p>
                                    </div>
                                </div>
                                <div className="col-md-6 col-sm-6">
                                    <div className="single-process">
                                        <img className="process-icon" src="assets/images/add.svg" alt="add"/>
                                        <h3 className="process-title">Add your nfts</h3>
                                        <p className="process-content">It is a long established fact reader will be
                                            distracted by the content ofpage when looking at its layout.</p>
                                    </div>
                                </div>
                                <div className="col-md-6 col-sm-6">
                                    <div className="single-process">
                                        <img className="process-icon" src="assets/images/clipboard.svg"
                                             alt="clipboard"/>
                                        <h3 className="process-title">List them for sale</h3>
                                        <p className="process-content">It is a long established fact reader will be
                                            distracted by the content ofpage when looking at its layout.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    )
}

export default HowToUseSection;