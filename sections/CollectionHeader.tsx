import Link from 'next/link';
import {formatCount} from "../src/lib/functions";
import {CopyAbleAddressComponent} from "../components/CopyAbleAddressComponent";
import { useAuth } from "../src/contexts/AuthProvider";
import {useMeQuery} from "../src/graphql/generated";
import {useState} from "react";

export function CollectionHeader({collection, itemCount}: any) {
    const { authState } = useAuth();
    const [me, setMe] = useState<any>(undefined);
     useMeQuery({}, {
        onSuccess: data => setMe(data.me)
    })
    return (
        <div className="collections-area section-top">
            <div className="container">
                <div className="collections-user">
                    <div className="row">
                        <div className="col-lg-4">
                            <div className="collections-left">
                                <div className="profile-area">
                                    <img className="author-image" src={collection?.image}
                                         alt={collection?.name}/>
                                </div>
                                {
                                    authState && me && me.id == collection?.author?.id &&
                                    <Link href={`/assets/create/${collection?.contractAddress}`}><span className="primary-btn cursor-pointer">Create Asset</span></Link>
                                }
                            </div>
                        </div>
                        <div className="col-lg-8">
                            <div className="user-info">
                                <h3 className="user-name">{collection?.name}</h3>
                                <h4 className="created-by">Created by <Link href={`user/${collection?.author?.walletAddress}`}><a>@{collection?.author?.username}</a></Link></h4>
                                <CopyAbleAddressComponent address={collection?.contractAddress}/>
                                <p className="user-desc">{collection?.description}</p>
                                <div className="user-meta-info">
                                    <ul>
                                        <li>
                                            <span>{formatCount(itemCount)}</span>
                                            <span>items</span>
                                        </li>
                                        <li>
                                            <span>{formatCount(collection?.volumeTraded)}</span>
                                            <span>volume traded</span>
                                        </li>
                                    </ul>
                                </div>
                                <div className="collections-option">
                                    <ul>
                                        <li><a href="#"><img src="/assets/images/option-icon-1.svg" alt="option"/></a>
                                        </li>
                                        <li><a href="#"><img src="/assets/images/option-icon-2.svg" alt="option"/></a>
                                        </li>
                                        <li><a href="#"><img src="/assets/images/option-icon-3.svg" alt="option"/></a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}