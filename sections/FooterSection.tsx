import Link from "next/link";

const FooterSection = () => {
    return (
        <footer className="footer-area">
            <div className="widget-area">
                <div className="container">
                    <div className="row">
                        <div className="col-lg-3 col-md-6 col-sm-6">
                            <div className="single-widget about-widget">
                                <Link href="/"><a className="brand-logo"><img src="/assets/images/logo.png" alt="logo"/></a></Link>
                                <div className="newsletter-form mb-30">
                                    <h4 className="newsletter-title">Subscribe to our newsletter</h4>
                                    <form action="#">
                                        <div className="newsletter-form-group">
                                            <input type="email" className="form-control" id="newsletter_email"
                                                   name="newsletter_email"/>
                                            <button type="submit" className="newsletter-btn"><i
                                                className="fas fa-paper-plane"></i></button>
                                        </div>
                                    </form>
                                </div>
                                <ul className="social-media-area ">
                                    <li><a href="#"><i className="fab fa-twitter"></i></a></li>
                                    <li><a href="#"><i className="fab fa-facebook-f"></i></a></li>
                                    <li><a href="#"><i className="fab fa-instagram"></i></a></li>
                                    <li><a href="#"><i className="fab fa-github"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <div className="col-lg-3 col-md-6 col-sm-6">
                            <div className="single-widget menu-widget">
                                <h3 className="widget-title">Marketplace</h3>
                                <ul>
                                    <li><a href="#">All nfts</a></li>
                                    <li><a href="#">Live Auction</a></li>
                                    <li><a href="#">Tranding nfts</a></li>
                                    <li><a href="#">How its work</a></li>
                                </ul>
                            </div>
                        </div>
                        <div className="col-lg-3 col-md-6 col-sm-6">
                            <div className="single-widget menu-widget">
                                <h3 className="widget-title">My Account</h3>
                                <ul>
                                    <li><a href="#">Author</a></li>
                                    <li><a href="#">My account</a></li>
                                    <li><a href="#">Create account</a></li>
                                    <li><a href="#">Create collection</a></li>
                                </ul>
                            </div>
                        </div>
                        <div className="col-lg-3 col-md-6 col-sm-6">
                            <div className="single-widget contact-widget">
                                <h3 className="widget-title">Company</h3>
                                <p className="contact-address">5 North 03th Avenue,Penscola, FL 32503, New York</p>
                                <ul className="contact-info">
                                    <li><img className="icon" src="/assets/images/phone.svg" alt="phone"/> +1 965 047 658
                                        23
                                    </li>
                                    <li><img className="icon" src="/assets/images/envelope.svg"
                                             alt="envelope"/> xalkacom54@gmail.com
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className="footer-bottom">
                <div className="container">
                    <p className="copyright-text">&copy; 2021. All Rights Reserved by <a href="#">Itech</a></p>
                </div>
            </div>
        </footer>
    )
}

export default FooterSection;
