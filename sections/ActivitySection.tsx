export default function ActivitySection(){
    return(
        <>
            <div className="activity-page section">
                <div className="container">
                    <div className="short-field-area">
                        <form action="#">
                            <div className="short-field">
                                <select className="form-select">
                                    <option selected>Filter</option>
                                    <option value="1">Collection</option>
                                    <option value="2">Volume</option>
                                    <option value="3">Owners</option>
                                    <option value="4">Assets</option>
                                </select>
                            </div>
                        </form>
                    </div>
                    <div className="activity-list">
                        <div className="primary-table">
                            <div className="table-responsive">
                                <table className="table table-borderless">
                                    <tbody>
                                    <tr>
                                        <td>
                                            <div className="user-area">
                                                <img src="assets/images/author.png" alt="avater"
                                                     className="user-image"/>
                                                <div className="user-info">
                                                    <h5 className="user-desc">Creative Art collection</h5>
                                                    <h4 className="user-name"><a href="#">Alexander Garg</a></h4>
                                                </div>
                                            </div>
                                        </td>
                                        <td><span>2.81 ETH </span></td>
                                        <td><span>listed by</span> <a className="activity-id" href="#">@ayoub
                                            fennouni</a></td>
                                        <td><span className="space"></span></td>
                                        <td><span>Last 2 days</span></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div className="user-area">
                                                <img src="assets/images/author.png" alt="avater"
                                                     className="user-image"/>
                                                <div className="user-info">
                                                    <h5 className="user-desc">Creative Art collection</h5>
                                                    <h4 className="user-name"><a href="#">Alexander Garg</a></h4>
                                                </div>
                                            </div>
                                        </td>
                                        <td><span>2.81 ETH </span></td>
                                        <td><span>listed by</span> <a className="activity-id" href="#">@ayoub
                                            fennouni</a></td>
                                        <td><span className="space"></span></td>
                                        <td><span>Last 2 days</span></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div className="user-area">
                                                <img src="assets/images/author.png" alt="avater"
                                                     className="user-image"/>
                                                <div className="user-info">
                                                    <h5 className="user-desc">Creative Art collection</h5>
                                                    <h4 className="user-name"><a href="#">Alexander Garg</a></h4>
                                                </div>
                                            </div>
                                        </td>
                                        <td><span>2.81 ETH </span></td>
                                        <td><span>listed by</span> <a className="activity-id" href="#">@ayoub
                                            fennouni</a></td>
                                        <td><span className="space"></span></td>
                                        <td><span>Last 2 days</span></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div className="user-area">
                                                <img src="assets/images/author.png" alt="avater"
                                                     className="user-image"/>
                                                <div className="user-info">
                                                    <h5 className="user-desc">Creative Art collection</h5>
                                                    <h4 className="user-name"><a href="#">Alexander Garg</a></h4>
                                                </div>
                                            </div>
                                        </td>
                                        <td><span>2.81 ETH </span></td>
                                        <td><span>listed by</span> <a className="activity-id" href="#">@ayoub
                                            fennouni</a></td>
                                        <td><span className="space"></span></td>
                                        <td><span>Last 2 days</span></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div className="user-area">
                                                <img src="assets/images/author.png" alt="avater"
                                                     className="user-image"/>
                                                <div className="user-info">
                                                    <h5 className="user-desc">Creative Art collection</h5>
                                                    <h4 className="user-name"><a href="#">Alexander Garg</a></h4>
                                                </div>
                                            </div>
                                        </td>
                                        <td><span>2.81 ETH </span></td>
                                        <td><span>listed by</span> <a className="activity-id" href="#">@ayoub
                                            fennouni</a></td>
                                        <td><span className="space"></span></td>
                                        <td><span>Last 2 days</span></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}